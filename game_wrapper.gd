extends Node2D

@onready var title_screen: PackedScene = load("res://Screens/start_screen.tscn")
@onready var game_screen: PackedScene = load("res://Screens/world.tscn")
@onready var end_screen: PackedScene = load("res://Screens/end_screen.tscn")

var current_scene = null

# Called when the node enters the scene tree for the first time.
func _ready():
	current_scene = title_screen.instantiate()
	current_scene.connect("game_start", _game_start)
	
	add_child(current_scene)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _game_start():
	current_scene.queue_free()
	
	current_scene = game_screen.instantiate()
	add_child(current_scene)
	current_scene.connect("game_over", _game_over)

func _game_over(win: bool):
	current_scene.queue_free()
	current_scene = end_screen.instantiate()
	WinState.player_won = win
	add_child(current_scene)
	
	current_scene.connect("restart", _restart)

func _restart():
	current_scene.queue_free()
	
	current_scene = title_screen.instantiate()
	current_scene.connect("game_start", _game_start)
	
	add_child(current_scene)
	
