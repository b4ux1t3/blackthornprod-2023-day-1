extends Control


signal restart

func _ready():
	$VBoxContainer/WinLabel.visible = WinState.player_won
	$VBoxContainer/LossLabel.visible = !WinState.player_won

func _on_restart_button_pressed():
	restart.emit()


func _on_quit_button_pressed():
	get_tree().quit()
