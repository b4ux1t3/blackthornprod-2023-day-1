extends Control

signal game_start



func _on_play_button_pressed():
	game_start.emit()


func _on_exit_button_pressed():
	get_tree().quit()
