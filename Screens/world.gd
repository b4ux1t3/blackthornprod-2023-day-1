extends Node2D

signal game_over(win: bool)


func _on_sword_pickup_body_entered(body):
	if body is Player:
		game_over.emit(true)


func _on_enemy_wave_body_entered(body):
	if body is Player:
		game_over.emit(false)
