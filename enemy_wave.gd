extends Area2D

@export var speed: float = 200

func _process(delta):
	position.x += speed * delta
