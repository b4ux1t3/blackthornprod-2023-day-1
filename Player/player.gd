class_name Player extends CharacterBody2D


@export var speed: float = 300

# This is the height we junmp to while holding the jump button
@export var max_jump_height: float = 150
# This is the height we jump to if we just tap the jump button
@export var min_jump_height: float = 60
# This is how long we want it to take us to complete a jump
@export var jump_duration: float = 0.3

@export var falling_gravity_multiplier: float = 1.5

@export var coyote_time: float = 0.1
@export var jump_buffer_time: float = 0.1
@export var maximum_fall_speed: float = 3000
@export var maximum_acceleration: float = 8000
@export var friction: float = 15

@export var max_jump_amount = 1

@export var move_left_input := "move_left" 
@export var move_right_input := "move_right"
@export var joy_move_left_input := "joystick_left"
@export var joy_move_right_input := "joystick_right"

@export var jump_input := "jump"

enum JumpState {NO_JUMP, GROUND, AIR}

# We'l calculate these at the beginning of the game, based on ome formulae form the internet.
var gravity: float
var jump_velocity: float

# We might not use this, but it's trivial to define and calculate. 
# Maybe another dev will want to add double jump :)
var double_jump_velocity: float
var jump_termination_gravity: float

# This tells us if the player can jump, more or less
var jumps_left: int

# Ued by the jump buffer
var holding_jump: bool

var was_on_ground: bool

var current_jump_state: JumpState = JumpState.NO_JUMP

var current_acceleration: Vector2

@onready var coyote_timer = Timer.new()
@onready var jump_buffer_timer = Timer.new()

func _joystick_movement() -> float:
	return Input.get_axis("joystick_left", "joystick_right")

func _input(event):
	current_acceleration.x = 0
	var joystick_movement = _joystick_movement()
	if abs(joystick_movement) > 0.1:
		current_acceleration.x = joystick_movement * maximum_acceleration
	else:
		current_acceleration.x = Input.get_axis(move_left_input, move_right_input) * maximum_acceleration
	
	if event.is_action_pressed(jump_input):
		holding_jump = true
		jump_buffer_timer.start()
	if event.is_action_released(jump_input):
		holding_jump = false

func _ready():
	add_child(coyote_timer)
	coyote_timer.wait_time = coyote_time
	coyote_timer.one_shot = true
	
	add_child(jump_buffer_timer)
	jump_buffer_timer.wait_time = jump_buffer_time
	jump_buffer_timer.one_shot = true
	
	gravity = (2 * max_jump_height) / jump_duration ** 2
	jump_velocity = (2 * max_jump_height) / jump_duration
	jump_termination_gravity = (jump_velocity ** 2 / (2 * min_jump_height)) / gravity
	
	
func _physics_process(delta):
	_check_coyote_time()
	# We might jump here. . .
	_check_jump_buffer()
	# . . .or here
	_check_new_jump()
	
	var instantaneous_gravity = calculate_gravity(gravity)
	current_acceleration.y = instantaneous_gravity
	
	velocity.x *= calculate_friction(delta)
	if velocity.x > 0:
		$PlayerArt.flip_h = false
	if velocity.x < 0:
		$PlayerArt.flip_h = true
		
	velocity += current_acceleration * delta
	velocity.y = maximum_fall_speed if velocity.y > maximum_fall_speed else velocity.y
	was_on_ground = is_on_floor()

	move_and_slide()

func _check_coyote_time():
	if is_on_floor():
		coyote_timer.start()
	if not coyote_timer.is_stopped():
		jumps_left = max_jump_amount

func _check_jump_buffer():
	if not was_on_ground and is_on_floor():
		current_jump_state = JumpState.NO_JUMP
		
		if not jump_buffer_timer.is_stopped():
			jump()

func _check_new_jump():
	if Input.is_action_pressed(jump_input) and can_jump():
		jump()

func can_jump() -> bool:
	return jumps_left > 0 and is_on_floor()

func jump():
	if not is_on_floor() and coyote_timer.is_stopped():
		jumps_left = 0
		
	if jumps_left > 0:
		velocity.y = -jump_velocity
		current_jump_state = JumpState.GROUND if is_on_floor() else JumpState.AIR
		jumps_left -= 1

	coyote_timer.stop()

func calculate_gravity(grav) -> float:
	if velocity.y > 0:
		grav *= falling_gravity_multiplier
	elif velocity.y < 0 and not holding_jump:
		grav *= jump_termination_gravity
	
	return grav

func calculate_friction(delta: float):
	return 1 / (1 + (delta * friction))
